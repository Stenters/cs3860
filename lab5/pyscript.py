import pymysql
import statistics
import math
import operator
import re

# Create a connection to the database
connection = pymysql.connect(host='localhost',
                             user='root',
                             password='myasscrack',
                             db='data_analytics_2017',
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)

# Create a cursor object for executing SQL queries and obtaining results
db = connection.cursor()

def get_optimal_alloc():
    """ 
    Get the optimal % to allocate to each stock in the portfolio
    Based upon the Sharpe value of the weighted portfolio
    """
    # A dict to store the results in
    res = {'weights': [], 'sharpe': [], 'return': []}

    # For every valid permutation of stock weights, calculate the sharpe value
    # and cumulative return
    # (note: values are multiplied by 10 initially to allow for integer steps)
    for i in range(11):
        for j in range(11):
            for k in range(11):
                for l in range(11):

                    # Ensure you aren't overwriting iterators
                    (g,n,c,f) = (i,j,k,l)
                    
                    # If weights don't sum to 1, do nothing
                    if (g + n + c + f != 10):
                        pass 

                    else:
                        # Normalize values
                        g /= 10
                        n /= 10
                        c /= 10
                        f /= 10

                        # Calculate the sharpe value and cumulative return of the weight
                        (s_val, cum_ret) = simulation('2016-10-06', '2017-10-03', [g,n,c,f])

                        # Save the results
                        res['weights'].append([g,n,c,f])
                        res['sharpe'].append(s_val)
                        res['return'].append(cum_ret)

    # Get the index of the maximum sharpe value
    i = max(enumerate(res['sharpe']), key=operator.itemgetter(1))[0]

    # Print results
    print(f"optimal weight is: {res['weights'][i]} with a sharpe value of {res['sharpe'][i]}, and a cumulative return of {res['return'][i]}")

def simulation(start_date = None, end_date = None, weights = None):
    """
    Get the sharpe value and cumulative return of a weighted portfolio

    :return: a tuple containing the sharpe value and cumulative return
    """
    # Unless given, ask for parameters
    if start_date == None:
        (start_date, end_date, weights) = get_Valid_Input()

    # Initialize result objects
    pCR = []
    sCR = []

    # Get the data from the database
    data = get_Data(start_date, end_date)

    # Foreach result, save the weighted value and S.P.Y. value
    for d in data:
        # get the weighted stock values
        g = d['goog_Cumulative_Return'] * weights[0]
        n = d['nvda_Cumulative_Return'] * weights[1]
        c = d['celg_Cumulative_Return'] * weights[2]
        f = d['fb_Cumulative_Return']  *  weights[3]
        # Save overall portfolio value and S.P.Y. value
        pCR.append(g + n + c + f)
        sCR.append(d['spy_Cumulative_Return'])

    # Calculate the sharpe value and cumulative return of the weighted portfolio
    s_val = sharpe(pCR, sCR)
    cum_ret = calc_cum_ret(pCR[0], pCR[-1])
    # Return
    return (s_val, cum_ret)


def get_Valid_Input():
    """
    Get and validate user input

    :return: a tuple containing the start date, end date, and weights to use
    """
    # Get and validate
    start_date = get_Date("Start Date")
    end_date = get_Date("End Date")
    weights = get_weights()

    # Return
    return (start_date, end_date, weights)


### Helper Methods ###
    
def get_Date(title):
    """
    Use regular expressions to get a valid date input from the user

    :param title: the title to prompt the user for
    :return: a string containing the validated date
    """
    dateRE = re.compile(r'\d{4}-\d{2}-\d{2}')
    is_done = False
    while not is_done:
        date = input(f"Enter {title} (YYYY-mm-dd): ")
        is_done = re.match(dateRE, date)
        if not is_done:
            print(f"Invalid format for {title}")
    
    return date

def get_weights():
    """
    Get an array of the weights the user desires to use, entered in form [a,b,c,d]

    :return: list of weights as floats
    """
    weightRE = re.compile(r'(.\d+\s*|0\s*){4}')
    is_done = False
    
    while not is_done:
        weights = input("Enter weights for stocks in order goog, nvda, celg, fb, spy (split by spaces, must sum to 1):\n")
        res = list(map(float, re.match(weightRE, weights).group().rstrip().split(' ')))
        is_done = math.isclose(sum(res), 1)
        if not is_done:
            print("Does not sum to 1!")

    return list(map(float, res))

def get_Data(start, end):
    """
    Get all data from the database with records fall inside specified date range

    :param start: the start date to fetch from
    :param end: the end date to fetch to
    """
    sql = f"SELECT * FROM portfolio where date between '{start}' and '{end}';"
    db = connection.cursor() # TODO: Why?
    db.execute(sql)
    return db.fetchall()

def sharpe(port_cum_ret, spy_cum_ret):
    """
    Get the sharpe value of a portfolio given the cumulative return of the
    portfolio and the cumulative return of the S.P.Y.

    :param port_cum_ret: the cumulative return of the portfolio
    :param spy_cum_ret: the cumulative return of the S.P.Y.
    """
    avg = 0
    std_dev = []
    n = len(port_cum_ret)

    # Find the average and standard deviation of the data
    for i in range(n):
        avg += (port_cum_ret[i] - spy_cum_ret[i])
        std_dev.append(port_cum_ret[i] - spy_cum_ret[i])
    
    avg /= n
    std_dev = statistics.pstdev(std_dev)

    # S = n^(.5) * avg(Ra - Rb) / std_dev(Ra - Rb)
    #   Ra = portfolio cumulative return
    #   Rb = spy cumulative return
    return (n**.5 * avg) / std_dev

def calc_cum_ret(last_daily_val, first_daily_val):
    """
    Method for getting the normalized value by comparing it to the first value
    
    :param last_daily_val: last value in data set
    :param first_daily_val: first value in data set
    """
    return (last_daily_val - first_daily_val) / first_daily_val

if __name__ == '__main__':
    ret = input("(s)imulation or (o)ptimize: ")
    [simulation() if ret == 's' else get_optimal_alloc()]
    

