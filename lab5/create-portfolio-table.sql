DROP TABLE IF EXISTS portfolio;

CREATE TABLE portfolio(
  date date,
  goog_Adjusted_Close float, goog_Cumulative_Return float, goog_Value float,
  nvda_Adjusted_Close float, nvda_Cumulative_Return float, nvda_Value float,
  celg_Adjusted_Close float, celg_Cumulative_Return float, celg_Value float,
  fb_Adjusted_Close float,   fb_Cumulative_Return float,   fb_Value float,
  spy_Adjusted_Close float,  spy_Cumulative_Return float,  spy_Value float,
  portfolio_Cumulative_Return float, portfolio_value float
);

INSERT INTO portfolio
  (date, goog_Adjusted_Close, nvda_Adjusted_Close,
    celg_Adjusted_Close, fb_Adjusted_Close, spy_Adjusted_Close)
SELECT g.date, g.close, n.close, c.close, f.close, s.close
FROM goog g
INNER JOIN nvda n on g.date = n.date
INNER JOIN celg c on g.date = c.date
INNER JOIN fb f   on g.date = f.date
INNER JOIN spy s  on g.date = s.date
ORDER BY date ASC;

UPDATE portfolio p1
INNER JOIN portfolio a ON a.date = "2016-10-06"
SET p1.goog_Cumulative_Return = p1.goog_Adjusted_Close / a.goog_Adjusted_Close,
    p1.nvda_Cumulative_Return = p1.nvda_Adjusted_Close / a.nvda_Adjusted_Close,
    p1.celg_Cumulative_Return = p1.celg_Adjusted_Close / a.celg_Adjusted_Close,
    p1.fb_Cumulative_Return   = p1.fb_Adjusted_Close   / a.fb_Adjusted_Close,
    p1.spy_Cumulative_Return  = p1.spy_Adjusted_Close  / a.spy_Adjusted_Close;
