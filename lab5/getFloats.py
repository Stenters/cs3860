# Prompt the user to enter a space separated list of floats, then check that the sum is equal to 1.0. Loop until true
def get_Valid_Allocation():
  while not math.isclose((sum(map(float, re.findall(r'\s*(\.\d+)\s*', input("Enter portfolio allocation (Separrated by spaces): "))))), 1.0): pass
