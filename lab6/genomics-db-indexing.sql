# CS 3860
#
# genomics-db-indexing.sql
#

create schema genomics;

# record variables
SHOW VARIABLES LIKE 'max_heap_table_size';
# 16777216
SHOW VARIABLES LIKE 'tmp_table_size';
# 16777216

select @@max_heap_table_size;
# 16777216
select @@tmp_table_size;
# 16777216

# set global heap size to 2G
set @@max_heap_table_size=1024 * 1024 * 1024 * 2;
# 2147483648
set @@tmp_table_size=1024 * 1024 * 1024 * 2;

select @@max_heap_table_size;
# 2147483648
select @@tmp_table_size;
# 2147483648

#set @@max_heap_table_size=1024 * 1024 * 16;
#set @@max_heap_table_size=1024 * 1024 * 16;

#SET GLOBAL tmp_table_size = 1024 * 1024 * 1024 * 2;
#SET GLOBAL max_heap_table_size = 1024 * 1024 * 1024 * 2;


# create gene_info
drop table if exists gene_info; 
create table gene_info ( 
tax_id int, 
GeneID int, 
Symbol varchar(48), 
LocusTag varchar(48), 
Synonyms varchar(1000), 
dbXrefs varchar(512), 
chromosome varchar(48), 
map_location varchar(48), 
description varchar(4000), 
type_of_gene varchar(48), 
Symbol_from_nomenclature_authority varchar(64), 
Full_name_from_nomenclature_authority varchar(256), 
Nomenclature_status varchar(24), 
Other_designations varchar(4000), 
#Modification_date varchar(24)) ENGINE=INNODB; 
#Modification_date varchar(24)) ENGINE=MYISAM; 
Modification_date varchar(24)) ENGINE=MEMORY; 

# load
#Load Data local Infile 

Load Data local Infile 
'C:/Users/enterss/Projects/CS3860/Lab6/gene_info50000.csv' into table 
genomics.gene_info fields terminated by ',' lines terminated by '\n' ignore 1 lines; 

# record the number of records loaded

# verify
desc gene_info;
#select * from gene_info order by tax_id;

drop table if exists gene2pubmed; 
create table gene2pubmed ( 
tax_id int, 
GeneID int, 
#PMID int) ENGINE=INNODB;
#PMID int) ENGINE=MYISAM;
PMID int) ENGINE=MEMORY;

Load Data local Infile 
'C:/Users/enterss/Projects/CS3860/Lab6/gene2pubmed' into table 
genomics.gene2pubmed fields terminated by '\t' lines terminated by '\n' IGNORE 1 LINES; 

# record the number of records loaded

# verify
desc gene2pubmed;
#select * from gene2pubmed limit 100;

# note row count
select count(*) from gene_info;
select count(*) from gene2pubmed;

# Q1 - join
# first, set profiling on
set profiling=0; # off
set profiling=1; # on
select * 
from gene_info gi, gene2pubmed gp
where gi.geneid=gp.geneid
and gi.geneid=4126706;
# list each query with duration 
show profiles;

# note execution time

# analyze query plan
explain select * 
from gene_info gi, gene2pubmed gp
where gi.geneid=gp.geneid
and gi.geneid=4126706;

# record what keys are being used and the number of rows accessed from each table

# record your observations!

#lets try adding primary keys 
alter table gene_info add constraint primary key (geneid);

alter table gene2pubmed add constraint primary key (pmid, geneid);

# record the time to create each primary key

# re-execute the same query

set profiling=0;
set profiling=1;
select * 
from gene_info gi, gene2pubmed gp
where gi.geneid=gp.geneid
and gi.geneid=4126706;
show profiles;

# note execution time

# analyze query plan
explain select * 
from gene_info gi, gene2pubmed gp
where gi.geneid=gp.geneid
and gi.geneid=4126706;

# record what keys are being used and the number of rows accessed from each table

# record your observations! Why is the PK not being used for gene2pubmed??

# lets re-create the gene2pubmed PK based on join criteria.

alter table gene2pubmed drop primary key;
alter table gene2pubmed add constraint primary key (geneid, pmid);

# record times 

# re-execute the same query

set profiling=1;
select * 
from gene_info gi, gene2pubmed gp
where gi.geneid=gp.geneid
and gi.geneid=4126706;
show profiles;

# note execution time

# analyze query plan
explain select * 
from gene_info gi, gene2pubmed gp
where gi.geneid=gp.geneid
and gi.geneid=4126706;

# record what keys are being used and the number of rows accessed from each table

# record your observations! 

####################################################################

# Q2 - restriction
set profiling=0;
set profiling=1;
select * 
from gene_info gi
where gi.locustag='p49879_1p15';
show profiles;

# note execution time

# analyze query plan
explain select * 
from gene_info gi
where gi.locustag='p49879_1p15';

# record what keys are being used and the number of rows accessed from each table

# record your observations

# create index
create index gene_info_locustag on gene_info( locustag );

# record time

# re-execute the query

set profiling=0;
set profiling=1;
select * 
from gene_info gi
where gi.locustag='p49879_1p15';
show profiles;

# note execution time

# analyze query plan
explain select * 
from gene_info gi
where gi.locustag='p49879_1p15';

# record what keys are being used and the number of rows accessed from each table

# record your observations


####################################################################

# Q3 - range query
set profiling=0;
set profiling=1;
select * 
from gene_info
where geneid between '5961931' and '5999886';
show profiles;

# note execution time

# analyze query plan
explain select * 
from gene_info
where geneid between '5961931' and '5999886';

# record what keys are being used and the number of rows accessed from each table

# record your observations

####################################################################

# Q4 - insert
set profiling=0;
set profiling=1;
insert into gene2pubmed values (9606, 5555, 6666);
show profiles;

# note execution time

# analyze query plan
explain insert into gene2pubmed values (9606, 5555, 6666);

# record what keys are being used and the number of rows accessed from each table

# record your observations

####################################################################

# Q5 - update
set profiling=0;
set profiling=1;
update gene_info set locustag='No Locus Tag' where locustag='-';
show profiles;

# note execution time

# analyze query plan
explain update gene_info set locustag='No Locus Tag' where locustag='-';

# record what keys are being used and the number of rows accessed from each table

# record your observations



