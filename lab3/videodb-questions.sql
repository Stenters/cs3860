--  Q1
SELECT *
FROM Video_Recordings, Video_Categories
LIMIT 10;
SELECT count(*) 'Rows Affected'
FROM Video_Recordings, Video_Categories;

--  Q2
SELECT *
FROM Video_Recordings vr
INNER JOIN Video_Categories vc
WHERE vr.category_id = vc.id
LIMIT 10;
SELECT count(*) 'Rows Affected'
FROM Video_Recordings vr, Video_Categories vc
WHERE vr.category_id = vc.id;


-- Q3
SELECT vc.name, count(*)
FROM Video_Recordings vr
INNER JOIN Video_Categories vc ON vr.category_id = vc.id
GROUP BY vc.name;

-- Q4
SELECT vc.name, count(*)
FROM Video_Recordings vr
INNER JOIN Video_Categories vc ON vr.category_id = vc.id
WHERE vr.stock_count > 1
GROUP BY vc.name;

-- Q5
SELECT va.name, vc.name, count(*)
FROM Video_Actors va
INNER JOIN Video_Actors_List al ON va.id = al.actor_id
INNER JOIN Video_Recordings vr ON al.recording_id = vr.id
INNER JOIN Video_Categories vc ON vr.category_id = vc.id
GROUP BY va.name
LIMIT 10;
SELECT count(*) 'Rows Affected'
FROM Video_Actors va
INNER JOIN Video_Actors_List al ON va.id = al.actor_id
INNER JOIN Video_Recordings vr ON al.recording_id = vr.id
INNER JOIN Video_Categories vc ON vr.category_id = vc.id;

-- Q6
SELECT va.name, count(*)
FROM Video_Actors va
INNER JOIN Video_Actors_List al ON va.id = al.actor_id
INNER JOIN Video_Recordings vr ON al.recording_id = vr.id
INNER JOIN Video_Categories vc ON vr.category_id = vc.id
GROUP BY va.name
HAVING count(*) > 1
LIMIT 10;
SELECT count(*) 'Rows Affected'
FROM (
  select count(*) c
  FROM Video_Actors va
  INNER JOIN Video_Actors_List al ON va.id = al.actor_id
  INNER JOIN Video_Recordings vr ON al.recording_id = vr.id
  INNER JOIN Video_Categories vc ON vr.category_id = vc.id
  GROUP BY va.name
  HAVING count(*) > 1
) a;


-- Q7


SELECT *
FROM Video_Actors va
WHERE va.name NOT IN (
  SELECT va.name
  FROM Video_Actors va
  INNER JOIN Video_Actors_List al ON va.id = al.actor_id
  INNER JOIN Video_Recordings vr ON al.recording_id = vr.id
  INNER JOIN Video_Categories vc ON vr.category_id = vc.id
  WHERE vc.name LIKE '"Comedy"'
)
GROUP BY va.name
LIMIT 10;
SELECT COUNT(*)
FROM Video_Actors va
WHERE va.name NOT IN (
  SELECT va.name
  FROM Video_Actors va
  INNER JOIN Video_Actors_List al ON va.id = al.actor_id
  INNER JOIN Video_Recordings vr ON al.recording_id = vr.id
  INNER JOIN Video_Categories vc ON vr.category_id = vc.id
  WHERE vc.name LIKE '"Comedy"'
);

-- Q8
SELECT va.name
FROM Video_Actors va
INNER JOIN Video_Actors_List al ON va.id = al.actor_id
INNER JOIN Video_Recordings vr ON al.recording_id = vr.id
INNER JOIN Video_Categories vc ON vr.category_id = vc.id
WHERE vc.name LIKE '"Comedy"'
AND va.name IN (
  SELECT va.name
  FROM Video_Actors va
  INNER JOIN Video_Actors_List al ON va.id = al.actor_id
  INNER JOIN Video_Recordings vr ON al.recording_id = vr.id
  INNER JOIN Video_Categories vc ON vr.category_id = vc.id
  WHERE vc.name LIKE '"Action & Adventure"'
);
