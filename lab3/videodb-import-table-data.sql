INSERT INTO Video_Ratings (name)
SELECT DISTINCT rating
FROM Video_Recordings_temp;

INSERT INTO Video_Directors (name)
SELECT DISTINCT director
FROM Video_Recordings_temp;

INSERT INTO Video_Categories (name)
SELECT DISTINCT name
FROM Video_Categories_temp;

INSERT INTO Video_Actors (name)
SELECT DISTINCT name
FROM Video_Actors_temp;

INSERT INTO Video_Recordings (id, rating_id, director_id, category_id, title,
  image_name, duration, year_released, price, stock_count)
SELECT DISTINCT r.recording_id, ra.id, d.id, c.id, r.title, r.image_name,
  r.duration, r.year_released, r.price, r.stock_count
FROM Video_Recordings_temp r
INNER JOIN Video_Ratings ra on r.rating = ra.name
INNER JOIN Video_Directors d on r.director = d.name
INNER JOIN Video_Categories c on r.category = c.name;

INSERT INTO Video_Actors_List (recording_id, actor_id)
SELECT at.recording_id, a.id
FROM Video_Actors_temp at
INNER JOIN Video_Actors a ON at.name = a.name;
