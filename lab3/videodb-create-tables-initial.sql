DROP TABLE IF EXISTS Video_Recordings_temp, Video_Categories_temp, Video_Actors_temp;

CREATE TABLE Video_Recordings_temp (
  recording_id int,
  director varchar(255),
  title varchar(255),
  category varchar(255),
  image_name varchar(255),
  duration float(11,2),
  rating varchar(255),
  year_released float(11,2),
  price float(11,2),
  stock_count float(11,2)
);


CREATE TABLE Video_Categories_temp (
  id int,
  name varchar(255)
);


CREATE TABLE Video_Actors_temp (
  id int,
  name varchar(255),
  recording_id int
);

LOAD DATA LOCAL INFILE "C:/Users/enterss/Projects/CS3860/Lab3/Video_Recordings.txt" INTO TABLE Video_Recordings_temp FIELDS TERMINATED BY '\t' OPTIONALLY ENCLOSED BY "" LINES TERMINATED BY '\r\n';
LOAD DATA LOCAL INFILE "C:/Users/enterss/Projects/CS3860/Lab3/Video_Categories.txt" INTO TABLE Video_Categories_temp FIELDS TERMINATED BY '\t' OPTIONALLY ENCLOSED BY "" LINES TERMINATED BY '\r\n';
LOAD DATA LOCAL INFILE "C:/Users/enterss/Projects/CS3860/Lab3/Video_Actors.txt" INTO TABLE Video_Actors_temp FIELDS TERMINATED BY '\t' OPTIONALLY ENCLOSED BY "" LINES TERMINATED BY '\r\n';
