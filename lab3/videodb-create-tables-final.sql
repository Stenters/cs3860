DROP TABLE IF EXISTS Video_Recordings, Video_Ratings, Video_Directors,
  Video_Categories, Video_Actors_List, Video_Actors;

CREATE TABLE Video_Ratings (
  id int NOT NULL AUTO_INCREMENT,
  name varchar(255),
  PRIMARY KEY (id)
) ENGINE=INNODB;

CREATE TABLE Video_Directors (
  id int NOT NULL AUTO_INCREMENT,
  name varchar(255),
  PRIMARY KEY (id)
) ENGINE=INNODB;

CREATE TABLE Video_Categories (
  id int NOT NULL AUTO_INCREMENT,
  name varchar(255),
  PRIMARY KEY (id)
) ENGINE=INNODB;

CREATE TABLE Video_Actors_List (
  recording_id int NOT NULL,
  actor_id int NOT NULL
) ENGINE=INNODB;

CREATE TABLE Video_Actors (
  id int NOT NULL AUTO_INCREMENT,
  name varchar(255),
  PRIMARY KEY (id)
) ENGINE=INNODB;


CREATE TABLE Video_Recordings (
  id int,
  rating_id int,
  director_id int,
  category_id int,
  title varchar(255),
  image_name varchar(255),
  duration float(11,2),
  year_released float(11,2),
  price float(11,2),
  stock_count float(11,2),
  PRIMARY KEY (id)
) ENGINE=INNODB;
