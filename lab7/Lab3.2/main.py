import boto3
import json

dynamodb = boto3.resource('dynamodb', region_name='us-east-2', 
aws_access_key_id = "AKIA3MBJNLEAQDYPTYU7", aws_secret_access_key = "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")
table = dynamodb.Table('Video')

def q1(table):
    """
    Execute select * from Video_Recordings, Video_Categories. 
    Note the cross-product effect of joining two tables. Record the number of rows generated. Do all permutations of Video_Recordings X Video_Categories make sense? Explain.
    """
    print("Not implemented")

def q2(table):
    """
    Execute select * from Video_Recordings vr, Video_Categories vc where vr.category=vc.name. 
    Note the cross-product effect of joining two tables when restricted on the appropriate keys. Record the number of rows generated. Explain the purpose of the join.
    """
    print("Not implemented")

def q3(table):
    """
    List the number of videos for each video category.
    """
    # Init objects
    categories = {}
    res = table.scan()

    # For each record, increment the relevent category count
    for record in res["Items"]:
        if record['category'] not in categories:
            categories.update({ record['category']: 0 })
        categories[record['category']] += 1

    # Print results
    print(categories)

def q4(table):
    """
    List the number of videos for each video category where the inventory is non-zero.
    """
    # Scan the table with parameters
    categories = {}
    res = table.scan(
        FilterExpression= "#sc > :num",
        ExpressionAttributeNames={ "#sc": "stock_count"},
        ExpressionAttributeValues={":num": 0}
    )

    # For each record, increment the relevent category count
    for record in res["Items"]:
        if record['category'] not in categories:
            categories.update({ record['category']: 0 })
        categories[record['category']] += 1

    # Print results
    print(categories)

def q5(table):
    """
    For each actor, list the video categories.
    """
    # Init objects
    actors = {}
    res = table.scan()

    # For each actor in each record, add them to the results
    # and add the category to each actor in that record
    for record in res["Items"]:
        for a in record["actors"]:
            if a not in actors:
                actors.update({ a: [] })

            if record["category"] not in actors[a]:
                actors[a].append(record["category"])


    # Print results
    for a in actors:
        print(f"{a}: {actors[a]}")

def q6(table):
    """
    Which actors have appeared in movies in different video categories?
    """
    # Init objects
    actors = {}
    res = table.scan()

    # For each actor in each record, add them to the results
    # and add the category to each actor in that record
    for record in res["Items"]:
        for a in record["actors"]:
            if a not in actors:
                actors.update({ a: [] })

            if record["category"] not in actors[a]:
                actors[a].append(record["category"])

    # Print results
    for a in actors:
        if len(actors[a]) > 1:
            print(f"{a}: {actors[a]}")

def q7(table):
    """
    Which actors have not appeared in a comedy?
    """
    # Init objects
    actors = {}
    res = table.scan()

    # For each actor in each record, add them to the results
    # and add the category to each actor in that record
    for record in res["Items"]:
        for a in record["actors"]:
            if a not in actors:
                actors.update({ a: [] })

            if record["category"] not in actors[a]:
                actors[a].append(record["category"])

    # Print results
    for a in actors:
        if "Comedy" not in actors[a]:
            print(f"{a}: {actors[a]}")

def q8(table):
    """
    Which actors have appeared in comedy and action adventure movies?
    """
    # Init objects
    actors = {}
    res = table.scan()

    # For each actor in each record, add them to the results
    # and add the category to each actor in that record
    for record in res["Items"]:
        for a in record["actors"]:
            if a not in actors:
                actors.update({ a: [] })

            if record["category"] not in actors[a]:
                actors[a].append(record["category"])

    # Print results
    for a in actors:
        if "Comedy" in actors[a] and "Action & Adventure" in actors[a]:
            print(f"{a}: {actors[a]}")

def main():
    """
    Run all tests selectively
    """

    num = input("Which test would you like to run? (enter to run all, q to quit): ")

    while num != 'q':
        try:
            # If user presses enter, run all queries
            if num == "":
                for i in range(1,9):
                    print(f"\n\tQ{i}")
                    exec(f"q{i}(table)")
            
            # Print the number the user entered if valid
            elif int(num) > 0 and int(num) < 9:
                print(f"\n\tQ{num}")
                exec(f"q{num}(table)")

            # Invalid entry
            else:
                print("Error, only numbers 1 - 8 are valid")

        except ValueError:
                print("You must enter a number")

        num = input("\n\nWhich test would you like to run? (enter to run all, q to quit): ")
            
if __name__ == "__main__":
    main()
