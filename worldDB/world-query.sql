# World Database Query File
#
# CS 3860

# 1) create a database schema 'world'
# 2) Execute the world-db.sql database dump script, This will drop existing tables and create 
# and populate a City, Country, and CountryLanguage database tables
# 3) Verify your import
desc Country;
desc CountryLanguage;
desc City;
select * from Country;
select * from CountryLanguage;
select * from City;

select CountryCode, Language from CountryLanguage;

select CountryCode, group_concat(Language) from CountryLanguage group by CountryCode;

# 4) Add indices:
drop index city_country on city; # index may already present from database dump
desc city;
create index city_country on city(CountryCode);
desc city;

# Queries from lecture:

# Basic SQL Query: from, where, select, group by, order by
# List official language by country
select CountryCode, Language, isOfficial
from CountryLanguage
where isOfficial='T';

# Find all languages spoken by all cities;
select *
from city c, countrylanguage cl;

select c.name, c.CountryCode, cl.Language
from city c, countrylanguage cl
where c.CountryCode=cl.CountryCode;

select *
from city c, countrylanguage cl
where c.CountryCode=cl.CountryCode;


# Select countries from specific Continents
# Use profiling, and explain to justify adding an index
drop index country_continent on Country; # may need to drop on additional passes through this script
set profiling=1;
select * 
from Country
where Continent in ( 'Asia', 'North America');
show profiles;

explain select * 
from Country
where Continent in ( 'Asia', 'North America');

create index country_continent on Country (Continent);

explain select * 
from Country
where Continent in ( 'Asia', 'North America');

set profiling=1;
select * 
from Country
where Continent in ( 'Asia', 'North America');
show profiles;

# equivalent continent query
select * 
from Country
where (Continent ='Asia' or Continent= 'North America');

# Multi-relation inner join
# List countries and official languages:
explain select c.Name as Ishkabible, cl.Language
from CountryLanguage cl, Country c
where cl.CountryCode=c.Code
and isOfficial='T';

# Multi-relation inner join, standard notation
# List countries and official languages:
select c.Name Country, cl.Language
from CountryLanguage cl inner join Country c on (cl.CountryCode=c.Code)
and isOfficial='T';

# Multi-relation inner join, with restriction (where)
# List languages for all countries in the Caribbean region:
select c.Name Country, cl.Language
from CountryLanguage cl, Country c
where (cl.CountryCode=c.Code
and isOfficial='T'
and Region='Caribbean');

# select functions: if, instr, column attribute renaming
# Find all countries in areas of North, Central, and South America and calculate surface area square miles from square kilometers.
# Note: Review functions: if, instr, truncate
select Country.Name as 'Country Name', IF(INSTR(Region, 'Central America'), Region, Continent) as Area,
truncate( SurfaceArea*(0.62137*0.62137), 2) as 'Area in Miles Squared'
from Country
where Continent like '%America%';

# select functions: if, instr, column attribute renaming
# Find all countries in areas of North, Central, and South America and calculate surface area square miles from square kilometers.
# Logical union with IN: Restrict to US OR Canada
select Name Country, IF(INSTR(Region, 'Central America'), Region, Continent) Area,
truncate( SurfaceArea*(0.62137*0.62137), 2) as 'Area in Miles Squared'
from Country 
where name in ('United States', 'Canada');

# select functions: if, instr, column attribute renaming
# Find all countries in areas of North, Central, and South America and calculate surface area square miles from square kilometers.
# Logical union with OR: Restrict to US OR Canada
select Name Country, IF(INSTR(Region, 'Central America'), Region, Continent) as Area,
truncate( SurfaceArea*(0.62137*0.62137), 2) as 'Area in Miles Squared'
from Country 
where (name = 'United States' or name = 'Canada');

# select functions: if, instr, column attribute renaming
# Find all countries in areas of North, Central, and South America and calculate surface area square miles from square kilometers.
# Logical union with UNION: Restrict to US OR Canada
select Name Country, IF(INSTR(Region, 'Central America'), Region, Continent) as Area,
truncate( SurfaceArea*(0.62137*0.62137), 2) as 'Area in Miles Squared'
from Country 
where name = 'United States'
UNION
select Name Country, IF(INSTR(Region, 'Central America'), Region, Continent) as Area,
truncate( SurfaceArea*(0.62137*0.62137), 2) as 'Area in Miles Squared'
from Country 
where name = 'Canada';

# Note on INTERSECT: use inner join,, or sub-query

# Aggregate functions: max, min, avg
select max(LifeExpectancy) max, min(LifeExpectancy) min, avg(LifeExpectancy) avg   
from Country;

# Aggregate functions: count() applied to all rows!
select count(*)
from City
where Population>1000000;

# Aggregate functions: count() applied to each group
select cl.Language, avg( LifeExpectancy) as 'Avg Life Expectancy' 
from country c, countrylanguage cl
where c.code=cl.CountryCode
and cl.isOfficial='T'
group by cl.language
order by avg( LifeExpectancy) desc;

# Aggregate functions: count() applied to each group with having
select cl.Language, avg( LifeExpectancy) as 'Avg Life Expectancy', count(*) n
from country c, countrylanguage cl
where c.code=cl.CountryCode
and cl.isOfficial='T'
group by cl.language
having n>2;

# Uncorrelated Nested Queries (inside out)
select c.name, c.SurfaceArea
from country c
where c.code IN (
select cl.CountryCode
from CountryLanguage cl
where Language='English'
);

# Uncorrelated Nested Queries (inside out) - true negation
select c.name, c.SurfaceArea
from country c
where c.code NOT IN (
select cl.CountryCode
from CountryLanguage cl
where Language='English'
);

# Correlated Nested Queries (inside out)
select c.name, c.SurfaceArea
from country c
where EXISTS (
select *
from CountryLanguage cl
where c.code=cl.CountryCode
and Language='English'
);

# Correlated Nested Queries (outside in), group by, order by 
select c.region, sum(c.SurfaceArea) as sa
from country c
where EXISTS (
select *
from CountryLanguage cl
where c.code=cl.CountryCode
and Language='English'
)
group by c.region
order by sa desc;

# Correlated Nested Queries (outside in), group by, order by - true negation
select c.name, c.SurfaceArea
from country c
where NOT EXISTS (
select *
from CountryLanguage cl
where c.code=cl.CountryCode
and Language='English'
);


