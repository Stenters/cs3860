import json

class MapReduce:
    def __init__(self):
        self.intermediate = {}
        self.result = []

    def emit_intermediate(self, key, value):
        self.intermediate.setdefault(key, [])
        self.intermediate[key].append(value)

    def emit(self, value):
        self.result.append(value) 

    def execute(self, data, mapper, reducer):
        for line in data:
            record = json.loads(line)
            mapper(record)

        for key in self.intermediate:
            reducer(key, self.intermediate[key])


        jenc = json.JSONEncoder()

### DEFAULT:
        self.result.sort(key=lambda x: x[1])
        for item in self.result:
            print(jenc.encode(item))

### FOR PROBLEM 1:
        # self.result.sort(key=lambda x: -x[1])
        # if len(self.result) > 5:
        #     for i in range(5):
        #         print(jenc.encode(self.result[i]))
        # else:
        #     for item in self.result:
        #         print(jenc.encode(item))

### FOR PROBLEM 2:
        # res = {'tiny':[], 'small':[], 'medium':[], 'big':[]}
        #
        # for item in self.result:
        #     item = item[0]
        #     if len(item) < 2:
        #         res['tiny'].append(item)
        #     elif len(item) < 5:
        #         res['small'].append(item)
        #     elif len(item) < 10:
        #         res['medium'].append(item)
        #     else:
        #         res['big'].append(item)

        # self.result.sort(key=lambda x: x[1])
        
        # for r in res:
        #     print([r, len(res[r])])